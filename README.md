# Getting Started with Create React App

This is a clone project of React TodosMVC example.

## Step by step working

### Step 1: Component Hierarchy
First divide the ui into different components. 
The image show how I divide the component from the mock of the application.
![mock](assets/mock.PNG)

- ToDoMVC (Black border)
    - ToDoText (Yellow border)
    - ToDoInput (Green border)
    - ToDoTable (Pink border) 
        * ToDoRow (Blue border)
        * ToDoTabRow (Dark Blue border)
    - ToDoInstruction (Red border)
### Step 2: Build Static Version of the App

We build from the ones lower (in the ToDoMVC) like ToDoInStruction. 

![draft](assets/Capture.PNG)

### Step 3: Identify the Minimal Representation of UI State

At this step we make the UI interactive, we need to be able to trigger changes to our underlying data model. React do this with State.

All the data in our application are
- The input text the user has entered
- The value checkbox of each to-do task
- The value of the to-do task list (where all the to-do tasks are kept )
- The filter list of to-do tasks

From those data, We need to define what is state based on 3 main questions about data.
1. Is it passed in from a parent via props? If so, it probably isn’t state.
2. Does it remain unchanged over time? If so, it probably isn’t state.
3. Can you compute it based on any other state or props in your component? If so, it isn’t state. 
So finally, our state is:
- The input text the user has entered for new todo item
- The value of the to-do task list (where all the to-do tasks are kept )
- The id of the todo which user want to change
- The new name of the todo which user want to change
- The value of the tag for filter the todo List

### Step 4: Identify Where Your State Should Live

In the application:
- ToDoInput needs to display the text. ToDoTable needs to add the todo item from the input to the todo list and filter the todo list based on ToDoTabRow
- The common owner component is ToDoMVC

### Step 5: Add Inverse Data Flow
After following all the step, we get the draft version of the app

![ver1](assets/ver1.PNG)

This version have the function similar to the todoMVC but the design is a bit off. In the next step, we need to make the design resemble the original one.

### Step 6: Style the website.
Apply CSS styling on the element of the website, we have the final version of the application TODOMVC. We also divide components of the web application.
![final](assets/final.PNG)


## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `yarn build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
