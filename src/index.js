import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import TodoInput from "./Components/Input.js";
import reportWebVitals from "./reportWebVitals";
import TodoList from "./Components/TodoList";

class TodoInStruction extends React.Component {
  render() {
    return (
      <div className="info">
        {" "}
        <p>Double-click to edit a Todo</p>
        <p>Cloned by nhathuy</p>
        <p>Part of TodoMVC</p>
      </div>
    );
  }
}

class TodoTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputText: "",
      task: [],
      idIsEditing: "",
      todoNameChange: "",
      filter: "All",
      editState: false,
    };
    this.handleInputTextChange = this.handleInputTextChange.bind(this);
    this.handleChangeTodo = this.handleChangeTodo.bind(this);
    this.handleIdToDoChange = this.handleIdToDoChange.bind(this);
    this.handleFilter = this.handleFilter.bind(this);
    this.handleTodoNameChange = this.handleTodoNameChange.bind(this);
    this.handleEditState = this.handleEditState.bind(this);
  }

  //Load the data to the task list
  componentDidUpdate(){
    localStorage.setItem('dataStore',JSON.stringify(this.state.task))
  }

  //Save the data to the local storage
  componentDidMount(){
    const dataStore = JSON.parse(localStorage.getItem('dataStore'));
    if(dataStore != null){
      this.setState({task: dataStore});
    }
  }

  //Handle input of the user
  handleInputTextChange(inputText) {
    this.setState({
      inputText: inputText,
    });
  }

  //Handle the change of the event (edit/delete/add)
  handleChangeTodo(task) {
    this.setState({
      task: task,
    });
  }

  //Handle the id of the todo is editing
  handleIdToDoChange(id = " ") {
    this.setState({ idIsEditing: id });
  }

  //Handle change of the filter
  handleFilter(filter) {
    this.setState({
      filter: filter,
    });
  }

  //Handle new name of the todo 
  handleTodoNameChange(todoNameChange) {
    this.setState({
      todoNameChange: todoNameChange,
    });
  }

  //Handle the edit state of the app
  handleEditState(editState) {
    this.setState({
      editState: !editState,
    });
  }

  render() {
    return (
      <div>
        <header>
          <h1 className="todoApp">todos</h1>
        </header>
        <div className="shadow">
          <TodoInput
            inputText={this.state.inputText}
            onInputTextChange={this.handleInputTextChange}
            task={this.state.task}
            onHandleChangeTodo={this.handleChangeTodo}
            filterTask={this.state.filterTask}
          />
          <TodoList
            task={this.state.task}
            onHandleChangeTodo={this.handleChangeTodo}
            idIsEditing={this.state.idIsEditing}
            onHandleIdToDoChange={this.handleIdToDoChange}
            filter={this.state.filter}
            onHandleFilter={this.handleFilter}
            todoNameChange={this.state.todoNameChange}
            onHandleTodoNameChange={this.handleTodoNameChange}
            editState={this.state.editState}
            onHandleEditState={this.handleEditState}
          />
        </div>

        <TodoInStruction task={this.state.task} />
      </div>
    );
  }
}

ReactDOM.render(<TodoTable />, document.getElementById("root"));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
