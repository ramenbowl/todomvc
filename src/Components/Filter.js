import React from "react";
import "../index.css";

class TodoTabFilter extends React.Component {
  constructor(props) {
    super(props);
    this.handleFilterChange = this.handleFilterChange.bind(this);
    this.handleDeleteTodo = this.handleDeleteTodo.bind(this);
  }

  //Change filter tag
  handleFilterChange(e) {
    e.preventDefault();
    this.props.onHandleFilter(e.target.value);
  }

  //Delete
  handleDeleteTodo() {
    this.props.onHandleChangeTodo(
      this.props.task.filter((todo) => todo.completed === false)
    );
  }

  render() {
    let numberOfTodo = this.props.task.filter(
      (item) => item.completed === false
    );

    let numberOfComplete = this.props.task.filter(
      (item) => item.completed === true
    );

    return numberOfTodo.length > 0 || this.props.task.length > 0 ? (
      <div className="tag">
        <li className="filter">
          <span className="countTodo">
            {" "}
            {this.props.filter !== "Completed"
              ? numberOfTodo.length
              : numberOfComplete.length}{" "}
            Item left
          </span>
          <div className = "filterContainer">
            <button
              className={
                this.props.filter === "All" ? "filterTagAfter" : "filterTag"
              }
              onClick={this.handleFilterChange}
              value="All"
            >
              All{" "}
            </button>
            <button
              className={
                this.props.filter === "Active" ? "filterTagAfter" : "filterTag"
              }
              onClick={this.handleFilterChange}
              value="Active"
            >
              Active{" "}
            </button>
            <button
              className={
                this.props.filter === "Completed"
                  ? "filterTagAfter"
                  : "filterTag"
              }
              onClick={this.handleFilterChange}
              value="Completed"
            >
              Completed{" "}
            </button>
          </div>
          <button
            className={numberOfComplete.length > 0 ? "clear" : "clearHide"}
            onClick={this.handleDeleteTodo}
            value="Clear Completed"
          >
            Clear Completed{" "}
          </button>
        </li>
      </div>
    ) : (
      <div></div>
    );
  }
}

export default TodoTabFilter;
