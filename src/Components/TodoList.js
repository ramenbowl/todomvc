import React from "react";
import "../index.css";
import TodoTabFilter from "./Filter";
import Todo from "./Todo";

class TodoList extends React.Component {
    constructor(props) {
      super(props);
      this.handleFilteredTodo = this.handleFilteredTodo.bind(this);
    }
  
    //Handle filter tag
    handleFilteredTodo(filterTag) {
      console.log(filterTag);
      switch (filterTag) {
        case "Completed":
          let a = this.props.task.filter((item) => item.completed === true);
          return a;
        case "Active":
          let b = this.props.task.filter((item) => item.completed === false);
          return b;
        default:
          let c = this.props.task;
          return c;
      }
    }
  
    render() {
      const filteredList = this.handleFilteredTodo(this.props.filter);
      console.log(this.props.task);
      return (
        <div>
          <ul className="list-todo">
            {filteredList.map((todo) => (
              <Todo
                //require key to work
                key={todo.key}
                text={todo.text}
                todoItem={todo}
                task={this.props.task}
                onHandleChangeTodo={this.props.onHandleChangeTodo}
                idIsEditing={this.props.idIsEditing}
                onHandleIdToDoChange={this.props.onHandleIdToDoChange}
                todoNameChange={this.props.todoNameChange}
                onHandleTodoNameChange={this.props.onHandleTodoNameChange}
                editState={this.props.editState}
                onHandleEditState={this.props.onHandleEditState}
              />
            ))}
          </ul>
          <TodoTabFilter
            task={this.props.task}
            onHandleChangeTodo={this.props.onHandleChangeTodo}
            filter={this.props.filter}
            onHandleFilter={this.props.onHandleFilter}
          />
        </div>
      );
    }
  }

export default TodoList;