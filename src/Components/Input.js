import React from "react";
import "../index.css";

class TodoInput extends React.Component {
  constructor(props) {
    super(props);
    this.handleInputTextChange = this.handleInputTextChange.bind(this);
    this.handleSubmitTodo = this.handleSubmitTodo.bind(this);
    this.handleCompleteAllTodo = this.handleCompleteAllTodo.bind(this);
  }

  //Handle input text of user
  handleInputTextChange(e) {
    console.log(e.target.value);
    this.props.onInputTextChange(e.target.value);
  }

  // Handle submit new todo
  handleSubmitTodo(e) {
    e.preventDefault();
    if (this.props.inputText !== "") {
      this.props.onHandleChangeTodo([
        ...this.props.task,
        //Use Random to create unique key
        {
          text: this.props.inputText,
          completed: false,
          key: Math.random() * 100,
        },
      ]);
      // debugger;
      this.props.onInputTextChange("");
    }
  }

  //Handle complete/uncomplete all tasks
  handleCompleteAllTodo() {
    let numberOfTodo = this.props.task.filter(
      (item) => item.completed === false
    );
    //Check how many tasks left and decide to complete/uncomplete
    numberOfTodo.length > 0
      ? this.props.onHandleChangeTodo(
          this.props.task.map((item) => {
            if (item.completed === false) {
              return {
                ...item,
                completed: true,
              };
            }
            return item;
          })
        )
      : this.props.onHandleChangeTodo(
          this.props.task.map((item) => {
            if (item.completed === true) {
              return {
                ...item,
                completed: false,
              };
            }
            return item;
          })
        );
    this.completeAll = !this.completeAll;
  }

  render() {
    let numberOfTodo = this.props.task.filter(
      (item) => item.completed === false
    );
    return (
      <div className="flex-container">
        {this.props.task.length > 0 ? (
          <span
            className={numberOfTodo.length > 0 ? "chooseAll" : "chooseAllTask"}
            onClick={this.handleCompleteAllTodo}
          >
            {">"}
          </span>
        ) : (
          <span className="chooseAllHide">{}</span>
        )}

        <form className="inputbox">
          <label>
            <input
              autoFocus={true}
              className="input"
              type="text"
              placeholder="What needs to be done?"
              value={this.props.inputText}
              onChange={this.handleInputTextChange}
            />
          </label>
          <input
            className="hide"
            onClick={this.handleSubmitTodo}
            type="submit"
            value="Submit"
          />
        </form>
      </div>
    );
  }
}

export default TodoInput;
