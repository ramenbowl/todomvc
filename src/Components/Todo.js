import React from "react";
import "../index.css";

class Todo extends React.Component {
  constructor(props) {
    super(props);
    this.myForm = React.createRef();
    this.handleDeleteTodo = this.handleDeleteTodo.bind(this);
    this.handleCompleteTodo = this.handleCompleteTodo.bind(this);
    this.handleEditTodo = this.handleEditTodo.bind(this);
    this.handleTodoChange = this.handleTodoChange.bind(this);
    this.handleTodoNameChange = this.handleTodoNameChange.bind(this);
    this.handleClickOut = this.handleClickOut.bind(this);
  }
  //Delete
  handleDeleteTodo() {
    this.props.onHandleChangeTodo(
      this.props.task.filter((todo) => todo.key !== this.props.todoItem.key)
    );
  }
  //Complete
  handleCompleteTodo() {
    this.props.onHandleChangeTodo(
      this.props.task.map((item) => {
        if (item.key === this.props.todoItem.key) {
          return {
            ...item,
            completed: !item.completed,
          };
        }
        return item;
      })
    );
  }

  //Edit
  handleEditTodo() {
    console.log("you choose to edit");
    this.props.onHandleEditState(this.props.editState);
    this.props.onHandleIdToDoChange(this.props.todoItem.key);
  }

  //Change to do
  handleTodoChange(e) {
    e.preventDefault();
    console.log("You change something");
    const newName =
      this.props.todoNameChange !== ""
        ? this.props.todoNameChange
        : this.props.todoItem.text;
    this.props.onHandleChangeTodo(
      this.props.task.map((item) => {
        if (item.key === this.props.todoItem.key) {
          return {
            ...item,
            text: newName,
          };
        }
        return item;
      })
    );
    this.props.onHandleEditState(this.props.editState);
    this.props.onHandleIdToDoChange("");
    this.props.onHandleTodoNameChange("");
  }

  //Set new name change for edit
  handleTodoNameChange(e) {
    console.log(e.target.value);
    this.props.onHandleTodoNameChange(e.target.value);
  }

  //Handle click out when edit
  handleClickOut(e) {
    console.log("234");
    if(this.props.todoNameChange !== ""){
      this.props.onHandleChangeTodo(
        this.props.task.map((item) => {
          if (item.key === this.props.todoItem.key) {
            return {
              ...item,
              text: this.props.todoNameChange,
            };
          }
          return item;
        })
      );
    }
    this.props.onHandleTodoNameChange("");
    this.props.onHandleIdToDoChange("");
  }

  render() {
    const index = this.props.todoItem.key;
    const isEditing = this.props.editState;
    return (
      <div className="flex-container">
        <div
          className={
            !isEditing || this.props.idIsEditing !== this.props.todoItem.key
              ? "round"
              : "roundHidden"
          }
        >
          <input
            onClick={this.handleCompleteTodo}
            type="checkbox"
            id={"checkbox" + index}
            checked={this.props.todoItem.completed}
            onChange={this.handleCompleteTodo}
          />
          <label htmlFor={"checkbox" + index}></label>
        </div>
        {this.props.idIsEditing === this.props.todoItem.key && (
          <div className="flex-container">
            <form>
              <label>
                <input
                  autoFocus={true}
                  onBlur={this.handleClickOut}
                  className="editTodo"
                  type="text"
                  defaultValue={this.props.text}
                  onChange={this.handleTodoNameChange}
                />
              </label>
              <input
                className="hide"
                onClick={this.handleTodoChange}
                type="submit"
                value="Submit"
              />
            </form>
          </div>
        )}
        {this.props.idIsEditing !== this.props.todoItem.key && (
          <div className="flex-container">
            <li
              className={
                this.props.todoItem.completed ? "complete" : "todo-item"
              }
              onDoubleClick={this.handleEditTodo}
            >
              {this.props.text}
            </li>
            <button className="deleteTodo" onClick={this.handleDeleteTodo}>
              x
            </button>
          </div>
        )}
      </div>
    );
  }
}

export default Todo;
